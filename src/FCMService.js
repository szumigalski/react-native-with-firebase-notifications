import firebase from 'react-native-firebase';
import type {Notification, NotificationOpen} from 'react-native-firebase';

class FCMService {
  register = (onRegister, onNotification, onOpenNotification) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
    );
  };

  checkPermission = (onRegister) => {
    firebase
      .messaging()
      .hasPermission()
      .then((enabled) => {
        if (enabled) {
          this.getToken(onRegister);
        } else {
          this.requestPermission(onRegister);
        }
      })
      .catch((error) => {
        console.log('Permission rejected ', error);
      });
  };

  getToken = (onRegister) => {
    firebase
      .messaging()
      .getToken()
      .then((fcmToken) => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log('User does not have a device token');
        }
      })
      .catch((error) => {
        console.log('getToken rejected ', error);
      });
  };

  requestPermission = (onRegister) => {
    firebase
      .messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch((error) => {
        console.log('Request rejected ', error);
      });
  };

  deleteToken = () => {
    firebase
      .messaging()
      .deleteToken()
      .catch((error) => {
        console.log('Delete token error ', error);
      });
  };

  createNotificationListeners = (
    onRegister,
    onNotification,
    onOpenNotification,
  ) => {
    this.notificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        onNotification(notification);
      });

    this.notificationOpenListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        onOpenNotification(notificationOpen);
      });

    firebase
      .notifications()
      .getInitialNotification()
      .then((notificationOpen) => {
        if (notificationOpen) {
          if (notificationOpen) {
            const notification: Notification = notificationOpen.notification;
            onOpenNotification(notification);
          }
        }
      });

    this.messageListener = firebase.messaging().onMessage((message) => {
      onNotification(message);
    });

    this.onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh((fcmToken) => {
        console.log('new token: ', fcmToken);
        this.onRegister(fcmToken);
      });
  };

  unRegister = () => {
    this.notificationListener();
    this.notificationOpenListener();
    this.messageListener();
    this.onTokenRefreshListener();
  };

  buildChannel = (obj) => {
    return new firebase.notifications.Android.Channel(
      obj.channelID,
      obj.channelName,
      firebase.notifications.Android.Importance.High,
    ).setDescription(obj.channelDes);
  };

  buildNotification = (obj) => {
    firebase.notifications().android.createChannel(obj.channel);

    return new firebase.notifications.Notification()
      .setSound(obj.sound)
      .setNotificationId(obj.dataId)
      .setTitle(obj.title)
      .setBody(obj.content)
      .setData(obj.data)
      .android.setChannelId(obj.channel.channelId)
      .android.setLargeIcon(obj.largeIcon)
      .android.setSmallIcon(obj.smallIcon)
      .android.setColor(obj.colorBgIcon)
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .android.setVibrate(obj.vibrate);
  };

  scheduleNotification = (notification, days, minutes) => {
    const date = new Date();
    if (days) {
      date.setDate(date.getDate() + days);
    }
    if (minutes) {
      date.setMinutes(date.getMinutes() + minutes);
    }

    firebase
      .notifications()
      .scheduleNotification(notification, {fireDate: date.getTime()});
  };

  displayNotification = (notification) => {
    firebase
      .notifications()
      .displayNotification(notification)
      .catch((err) => console.log('Display notification error: ', err));
  };

  removeDeliveredNotification = (notification) => {
    firebase
      .notifications()
      .removeDeliveredNotification(notification.notificationId);
  };
}

export const fcmService = new FCMService();
